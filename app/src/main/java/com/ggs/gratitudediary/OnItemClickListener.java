package com.ggs.gratitudediary;

import android.view.View;

public interface OnItemClickListener {
    void onItemClick(NoteAdapter.ViewHolder holder, View view, int position);
}

