package com.ggs.gratitudediary;

public interface OnBackPressedListener {
    void onBackButtonPressed();
}

