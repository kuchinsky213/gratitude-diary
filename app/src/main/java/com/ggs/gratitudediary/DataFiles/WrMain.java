package com.ggs.gratitudediary.DataFiles;

public class WrMain {
    private double temp;            // Temperature
    private double feels_like;      // Temperature
    private double temp_min;        // Minimum temperature
    private double temp_max;        // Maximum temperature
    private int pressure;           // Atmospheric pressure
    private int humidity;           // Humidity
}
