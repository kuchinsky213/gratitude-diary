package com.ggs.gratitudediary;

import android.graphics.Color;

import java.text.SimpleDateFormat;

public class AppConstants {

    // request codes in getting weather info
    public   static final int REQ_LOCATION_BY_ADDRESS = 101;
    public  static final int REQ_WEATHER_BY_GRID = 102;

    // request code in selecting how to get photo in the New menu
    public   static final int REQ_PHOTO_CAPTURE = 103;
    public static final int REQ_PHOTO_SELECTION = 104;

    // whether the existing photo is located in the New menu
    public  static final int CONTENT_PHOTO = 105;
    public   static final int CONTENT_PHOTO_EX = 106;

    // path of the saved picture
    public   static String FOLDER_PHOTO;

    // values for database in NoteDatabase
    public  static String DATABASE_NAME = "note.db";
    public  static String TABLE_NOTE = "NOTE";
    public  static int DATABASE_VERSION = 1;

    // whether new note is created or the existing note is modified
    public  static final int MODE_INSERT = 1;
    public  static final int MODE_MODIFY = 2;

    // number of notes per page in the List menu
    public static final int NUM_NOTES_PER_PAGE = 20;

    // sets colors for the charts in the Stat menu
    public static final int[] graphColor = {Color.rgb(171,90,184),
            Color.rgb(208,162,215), Color.rgb(157,227,227),
            Color.rgb(227,220,213), Color.rgb(230,156,182)
    };

    // date formats
    public  static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmm");
    public   static SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    public  static SimpleDateFormat dateFormat3 = new SimpleDateFormat("MM/dd");
    public  static SimpleDateFormat dateFormat4 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public   static SimpleDateFormat dateFormat5 = new SimpleDateFormat("yyyy-MM-dd");
public     static SimpleDateFormat dateFormat6 = new SimpleDateFormat("EEE, MMM d");
    public    static SimpleDateFormat dateFormat7 = new SimpleDateFormat("yyyy");
    public   static SimpleDateFormat dateFormat8 = new SimpleDateFormat("EEE, MMM d, a KK:mm");

    // date formats for Korean
    public  static SimpleDateFormat dateFormat9 = new SimpleDateFormat("M월 d일 EEE요일");
    public  static SimpleDateFormat dateFormat10 = new SimpleDateFormat("yyyy년");
    public  static SimpleDateFormat dateFormat11 = new SimpleDateFormat("M월 d일 EEE요일, a KK:mm");
    public  static SimpleDateFormat dateFormat12 = new SimpleDateFormat("dd/MM/yy");
   // public  static SimpleDateFormat dateFormat12 = new SimpleDateFormat("e;EEE, MMM d, ''yy");

}
